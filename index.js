//alert('ES6')

const getCube = 5**3
console.log(`The cube of 5 is ${getCube}.`);

const address = ['256', 'Gorordo Avenue', 'Cebu City', '6014']

const [streetNumber, streetName, city, postCode] = address;
console.log(`I live in ${streetNumber} ${streetName} ${city} ${postCode}`);

const animal = {
	animalName : 'Una',
	animalType : 'rabbit',
	color: 'white',
	species: 'long-eared mammals',
	mass: '1.2 kilograms',
	height: '8 inches'
}

const {animalName, animalType, color, species, mass, height} = animal;
console.log(`${animalName} is a ${color} ${animalType}. It belongs to the ${species} and it has a mass of ${mass} and a height of ${height}. `)

const randomNumbers = [2,5,7,3,6];

randomNumbers.forEach((random) => {
	console.log(random);
})


const reduceNumber = (x, y) => x + y;
console.log(randomNumbers.reduce(reduceNumber));


class Dog {
	constructor (name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed
	}
}
const myDog = new Dog('Flabby', '5', 'Labrador');
console.log(myDog);